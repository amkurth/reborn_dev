.. _theory:

Theory
======

We have started to collect some basic theory notes for students here.

- `Diffraction Equations <_static/dipole_html/dipole.html>`__ [ `pdf <_static/files/dipole.pdf>`__ ]
